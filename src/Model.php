<?php

class Model
{
    protected $pdo;

    public function __construct(array $config)
    {
        try {
            if ($config['engine'] == 'mysql') {
                $this->pdo = new \PDO(
                    'mysql:dbname='.$config['database'].';host='.$config['host'],
                    $config['user'],
                    $config['password']
                );
                $this->pdo->exec('SET CHARSET UTF8');
            } else {
                $this->pdo = new \PDO(
                    'sqlite:'.$config['file']
                );
            }
        } catch (\PDOException $error) {
            throw new ModelException('Unable to connect to database');
        }
    }

    /**
     * Tries to execute a statement, throw an explicit exception on failure
     */
    protected function execute(\PDOStatement $query, array $variables = array())
    {
        if (!$query->execute($variables)) {
            $errors = $query->errorInfo();
            throw new ModelException($errors[2]);
        }

        return $query;
    }

    /**
     * Inserting a book in the database
     */
    public function insertBook($title, $author, $synopsis, $image, $copies)
    {
        $query = $this->pdo->prepare('INSERT INTO livres (titre, auteur, synopsis, image)
            VALUES (?, ?, ?, ?)');
        $this->execute($query, array($title, $author, $synopsis, $image));
        $id = $this->pdo->lastInsertId();

        $query = $this->pdo->prepare('INSERT INTO exemplaires (book_id) 
            VALUES (?)');
        for ($i = 1; $i <= $copies; $i++)
            $this->execute($query, array($id));
    }

    /**
     * Getting all the books
     */
    public function getBooks()
    {
        $query = $this->pdo->prepare('SELECT livres.* FROM livres');

        $this->execute($query);

        return $query->fetchAll();
    }

    /**
     * Getting Idbooks à partir de ses données (pour les testes)
     */
    public function getIdBook($titre, $auteur, $synopsis)
    {
        $query = $this->pdo->prepare('SELECT livres.id FROM livres
            WHERE titre = ? AND auteur = ? AND synopsis = ?');

        $this->execute($query, array($titre, $auteur, $synopsis));

        return $query->fetch();
    }

    /**
     * Getting one book
     */
    public function getBook($id)
    {
        $query = $this->pdo->prepare('SELECT livres.* FROM livres WHERE livres.id = ?');

        $this->execute($query, array($id));

        return $query->fetch();
    }

    /**
     * Récupère les exemplaires INDISPONIBLE d'un livre (à partir de son Id) 
     */
    public function getExemplairesIndispoBook($id) {

        $query = $this->pdo->prepare('SELECT exemplaires.*, emprunts.* 
            FROM exemplaires join emprunts
            WHERE exemplaires.book_id = ?
            AND exemplaires.id = emprunts.exemplaire
            AND emprunts.fini = 0');

        $this->execute($query, array($id));

        return $query->fetchAll();
    }

    /**
     * Récupère les exemplaires DISPONIBLE d'un livre (à partir de son Id) 
     */
    public function getExemplairesDispoBook($id) {

        $query = $this->pdo->prepare('SELECT exemplaires.* FROM exemplaires
            WHERE exemplaires.book_id = ? 
            AND NOT EXISTS (SELECT * FROM emprunts WHERE emprunts.exemplaire = exemplaires.id 
                AND emprunts.fini = 0)');

        $this->execute($query, array($id));

        return $query->fetchAll();
    }

    /**
     * Récupère tous les emprunts effectués (retourner ou pas) 
     */
    public function getEmprunts() {

        $query = $this->pdo->prepare('SELECT emprunts.*, livres.titre 
            FROM emprunts join exemplaires join livres
            WHERE emprunts.exemplaire = exemplaires.id
            AND exemplaires.book_id = livres.id');

        $this->execute($query);

        return $query->fetchAll();
    }

    /**
     * Ajout un emprunt dans la base de donné
     */
    public function ajoutEmprunt($name, $exemplaire, $datefin) {

        $today = date('Y-m-d H:i:s');

        $query = $this->pdo->prepare('INSERT INTO emprunts (personne, exemplaire, debut, fin)
            VALUES (?, ?, ?, ?)');
        $this->execute($query, array($name, $exemplaire, $today, $datefin));

    }

    /**
     * Retourne un emprunt (indique la fin de l'emprunt)
     */
    public function retourneEmprunt($exemplaire) {

        $query = $this->pdo->prepare('UPDATE emprunts
            SET fini = 1
            WHERE exemplaire = ?');

        $this->execute($query, array($exemplaire));

    }

    /**
     * Getting IdExemplaire à partir de ses données (pour les testes)
     */
    public function getIdExemplaire($id)
    {
        $query = $this->pdo->prepare('SELECT exemplaires.id FROM exemplaires
            WHERE book_id = ?');

        $this->execute($query, array($id));

        return $query->fetchAll();
    }
}
