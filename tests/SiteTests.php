<?php

class SiteTests extends BaseTests
{
    /**
     * Testing that accessing the secure page doesn't works, and then logging
     */
    public function testAdmin()
    {
        $client = $this->createClient();

        // We are not admin, check that we get the error
        $crawler = $client->request('GET', '/addBook');
        $this->assertTrue($client->getResponse()->isOk());
        $this->assertCount(1, $crawler->filter('.shouldbeadmin'));

        // Logging in as admin, bad password
        $crawler = $client->request('POST', '/admin', ['login' => 'admin', 'password' => 'bad']);
        $this->assertTrue($client->getResponse()->isOk());
        $this->assertCount(0, $crawler->filter('.loginsuccess'));

        // Logging in as admin, success
        $crawler = $client->request('POST', '/admin', ['login' => 'admin', 'password' => 'password']);
        $this->assertCount(1, $crawler->filter('.loginsuccess'));

        // Now, we should get the page
        $crawler = $client->request('GET', '/addBook');
        $this->assertCount(0, $crawler->filter('.shouldbeadmin'));

        // Disconnect
        $crawler = $client->request('GET', '/logout');
        $this->assertTrue($client->getResponse()->isRedirect());
        $crawler = $client->request('GET', '/addBook');
        $this->assertCount(1, $crawler->filter('.shouldbeadmin'));
    }

    /**
     * Testing book insert (using form)
     */
    public function testBookInsertForm()
    {
        $client = $this->createClient();
        $this->app['session']->set('admin', true);

        // There is no book
        $books = $this->app['model']->getBooks();
        $this->assertEquals(0, count($books));

        // Inserting one using a POST request through the form
        $client->request('GET', '/addBook');
        $form = $client->getCrawler()->filter('form')->form();
        $form['title'] = 'Test';
        $form['author'] = 'Someone';
        $form['synopsis'] = 'A test book';
        $form['copies'] = 3;
        $client->submit($form);

        // There is one book
        $books = $this->app['model']->getBooks();
        $this->assertEquals(1, count($books));
    }

    /**
     * Testing emprunt
     */
    public function testEmprunt() {
        $client = $this->createClient();
        $this->app['session']->set('admin', true);

        //crée un livre en 3 exemplaires
        $client->request('GET', '/addBook');
        $form = $client->getCrawler()->filter('form')->form();
        $form['title'] = 'test livre trois exemplaires';
        $form['author'] = 'Quelqu un';
        $form['synopsis'] = 'C est un test pour voir si ca marche bien';
        $form['copies'] = 3;
        $client->submit($form);

        //récupère l'id du livre et d'un exemplaire
        $livre_id = $this->app['model']->getIdBook('test livre trois exemplaires', 
            'Quelqu un', 
            'C est un test pour voir si ca marche bien'
        );
        $exemplaire_id = $this->app['model']->getIdExemplaire($livre_id[0]);

        //vérifie que le nombre d'exemplaire disponible est bien de 3
        $nombredispo = $this->app['model']->getExemplairesDispoBook($livre_id[0]);
        $this->assertEquals(3, count($nombredispo));

        //emprunte un exemplaire
        $client->request('GET', '/emprunt/' . $livre_id[0] . '/' . $exemplaire_id[0][0]);
        $form = $client->getCrawler()->filter('form')->form();
        $form['name'] = 'HOARAU Christophe';
        $form['datefin'] = '01-01-2016';
        $client->submit($form);

        //vérifie que le nombre d'exemplaire disponible est passé à 2
        $nombredispo = $this->app['model']->getExemplairesDispoBook($livre_id[0]);
        $this->assertEquals(3, count($nombredispo));

        //retourne l'emprunt
        $this->app['model']->retourneEmprunt($exemplaire_id[0][0]);

        //vérifie que le nombre d'exemplaire disponible est repassé à 3
        $nombredispo = $this->app['model']->getExemplairesDispoBook($livre_id[0]);
        $this->assertEquals(3, count($nombredispo));
    }
}
