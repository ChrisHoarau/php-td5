<?php

return [
    // Database
    'database' => [
        'engine' => 'mysql',
        'host' => 'localhost',
        'database' => 'ProjetPhp',
        'user' => 'root',
        'password' => 'root'
    ],

    // Administrator auth
    'admin' => ['admin', 'password']
];
